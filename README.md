## SSR With ReactJS and NextJS.

## How it works
    - NextJS runs in nodejs server.
    - NextJS will start and execute the react code.
    - You can disable the js from browse to test, when it come from backend, we dont need js browser.

## Requirements 
    - NodeJS, npm or yarn.

## Start DEV
    - npm run start

## Generate build
    - npm run build

## start build generated
    - npm run start.

